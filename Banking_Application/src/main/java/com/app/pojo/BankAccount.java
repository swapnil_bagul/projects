package com.app.pojo;

public class BankAccount {

	private String name;
	private Long accountNumber;
	private String password;
	private String emailId;
	private Long mobileNumber;
	private String DOB;
	private String panNumber;
	private Long aadharNumber;
	private String city;

	public BankAccount() {
		super();
	}

	public BankAccount(String name, Long accountNumber, String password, String emailId, Long mobileNumber, String dOB,
			String panNumber, Long aadharNumber, String city) {
		super();
		this.name = name;
		this.accountNumber = accountNumber;
		this.password = password;
		this.emailId = emailId;
		this.mobileNumber = mobileNumber;
		DOB = dOB;
		this.panNumber = panNumber;
		this.aadharNumber = aadharNumber;
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public Long getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(Long aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "BankAccount [name=" + name + ", accountNumber=" + accountNumber + ", password=" + password
				+ ", emailId=" + emailId + ", mobileNumber=" + mobileNumber + ", DOB=" + DOB + ", panNumber="
				+ panNumber + ", aadharNumber=" + aadharNumber + ", address=" + city + "]";
	}

}
