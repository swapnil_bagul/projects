package com.app.pojo;

public class Transactions {
	private Integer transactionId;
	private String trasactionDate;
	private double accountBalance;
	private String trans_details;
	private Long account;

	public Transactions(Integer transactionId, String trasactionDate, double accountBalance, String trans_details,
			Long account) {
		super();
		this.transactionId = transactionId;
		this.trasactionDate = trasactionDate;
		this.accountBalance = accountBalance;
		this.trans_details = trans_details;
		this.account = account;
	}

	public Transactions() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public String getTrasactionDate() {
		return trasactionDate;
	}

	public void setTrasactionDate(String trasactionDate) {
		this.trasactionDate = trasactionDate;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getTrans_details() {
		return trans_details;
	}

	public void setTrans_details(String trans_details) {
		this.trans_details = trans_details;
	}

	public Long getAccount() {
		return account;
	}

	public void setAccount(Long account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "Transactions [transactionId=" + transactionId + ", trasactionDate=" + trasactionDate
				+ ", accountBalance=" + accountBalance + ", trans_details=" + trans_details + ", account=" + account
				+ "]";
	}

}
