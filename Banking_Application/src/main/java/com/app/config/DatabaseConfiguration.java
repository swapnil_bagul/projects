package com.app.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DatabaseConfiguration {

	public static void main(String[] args) {
		try {
			getDbConnection();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Connection getDbConnection() throws ClassNotFoundException, SQLException {
		Properties props = new Properties();
		FileReader reader = null;
		try {
			reader = new FileReader("./src/main/java/com/app/config/DbDetails.properties");
			props.load(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Connection con = null;
		if (con == null) {
			// Class.forName("com.mysql.cj.jdbc.Driver");
			Class.forName(props.getProperty("DriverClass"));
			// con =
			// DriverManager.getConnection("jdbc:mysql://localhost:3306/bankingapplication",
			// "root", "root");
			con = DriverManager.getConnection(props.getProperty("DbUrl"), props.getProperty("Username"),
					props.getProperty("Password"));
		}
		return con;
	}

	public static void closeConnection(Statement st, ResultSet rs, Connection con) throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (st != null) {
			st.close();
		}
		if (con != null) {
			con.close();
		}
	}

}
