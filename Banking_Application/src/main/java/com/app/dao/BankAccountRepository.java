package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.app.config.DatabaseConfiguration;
import com.app.exception.BankAccountException;
import com.app.pojo.BankAccount;

public class BankAccountRepository {

	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	PreparedStatement pst = null;

	public Map<Integer, String> getMenu() throws ClassNotFoundException, SQLException {
		LinkedHashMap<Integer, String> menuMap = new LinkedHashMap<>();
		if (con == null || con.isClosed()) {
			con = DatabaseConfiguration.getDbConnection();
		}
		st = con.createStatement();
		rs = st.executeQuery("Select *from menu");
		while (rs.next()) {
			menuMap.put(rs.getInt("MenuId"), rs.getString("MenuName"));
		}
		DatabaseConfiguration.closeConnection(st, rs, con);
		return menuMap;

	}

	public Long createNewAccount(BankAccount bankAccount)
			throws SQLException, ClassNotFoundException, BankAccountException {
		if (con == null || con.isClosed()) {
			con = DatabaseConfiguration.getDbConnection();
		}
		validateInputDetails(bankAccount);
		String sql = "insert into bank_account (accountNumber,name,password,emailId,mobileNumber,DOB,panCard,adharCard,city) "
				+ "values(?,?,?,?,?,?,?,?,?)";
		pst = con.prepareStatement(sql);
		pst.setLong(1, bankAccount.getAccountNumber());
		pst.setString(2, bankAccount.getName());
		pst.setString(3, bankAccount.getPassword());
		pst.setString(4, bankAccount.getEmailId());
		pst.setLong(5, bankAccount.getMobileNumber());
		pst.setString(6, bankAccount.getDOB());
		pst.setString(7, bankAccount.getPanNumber());
		pst.setLong(8, bankAccount.getAadharNumber());
		pst.setString(9, bankAccount.getCity());

		Integer i = pst.executeUpdate();
		con.commit();
		if (i == 0) {
			throw new BankAccountException("The account is not created...Please try again");
		}
		return bankAccount.getAccountNumber();
	}

	public boolean validateBankHolder(Long accountNumber, String password) throws ClassNotFoundException, SQLException {
		boolean exist = false;
		if (con == null || con.isClosed()) {
			con = DatabaseConfiguration.getDbConnection();
		}
		st = con.createStatement();
		rs = st.executeQuery(
				"select *from bank_account where accountNumber=" + accountNumber + " and password='" + password + "';");
		while (rs.next()) {
			if (rs.getLong("accountNumber") != 0 && rs.getString("password") != null) {
				exist = true;
			}
		}
		DatabaseConfiguration.closeConnection(st, rs, con);
		return exist;
	}

	public Map<Integer, String> loadLoginMenu() throws ClassNotFoundException, SQLException {
		LinkedHashMap<Integer, String> menuMap = new LinkedHashMap<>();
		if (con == null || con.isClosed()) {
			con = DatabaseConfiguration.getDbConnection();
		}
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("Select *from loginmenu");
		while (rs.next()) {
			menuMap.put(rs.getInt("MenuId"), rs.getString("MenuName"));
		}
		DatabaseConfiguration.closeConnection(st, rs, con);
		return menuMap;
	}

	private void validateInputDetails(BankAccount bankAccount) {
		try {
			if (bankAccount.getEmailId() != "") {
				Pattern p = Pattern.compile("^(.+)@(.+)$");
				Matcher match = p.matcher(bankAccount.getEmailId());
				if (!(match.find() && match.group().equals(bankAccount.getEmailId()))) {
					throw new BankAccountException("The Email Id is invalid");
				}
			} else {
				throw new BankAccountException("The Email Id is invalid");
			}
			if (bankAccount.getMobileNumber() != 0) {
				Pattern ptrn = Pattern.compile("(0/91)?[7-9][0-9]{9}");
				Matcher match = ptrn.matcher(bankAccount.getMobileNumber().toString());
				if (!(match.find() && match.group().equals(bankAccount.getMobileNumber().toString()))) {
					throw new BankAccountException("The Mobile number is invalid");
				}
			} else {
				throw new BankAccountException("The Mobile number is invalid");
			}
			if (bankAccount.getAadharNumber() != 0) {
				Pattern p = Pattern.compile("^[2-9]{1}[0-9]{3}[0-9]{4}[0-9]{4}$");
				Matcher match = p.matcher(bankAccount.getAadharNumber().toString());
				if (!(match.find() && match.group().equals(bankAccount.getAadharNumber().toString()))) {
					throw new BankAccountException("The aadhar number is invalid");
				}
			} else {
				throw new BankAccountException("The aadhar number is invalid");
			}
			if (bankAccount.getPanNumber() != "") {
				Pattern p = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
				Matcher match = p.matcher(bankAccount.getPanNumber());
				if (!(match.find() && match.group().equals(bankAccount.getPanNumber().toString()))) {
					throw new BankAccountException("The PAN number is invalid");
				}
			} else {
				throw new BankAccountException("The PAN number is invalid");
			}
		} catch (BankAccountException e) {
			System.out.println(e.getMessage());
		}
	}

	/*
	 * public static void main(String[] args) { BankAccount ba = new BankAccount();
	 * ba.setAadharNumber(866899454356L); ba.setPanNumber("CNCPN4656G");
	 * ba.setMobileNumber(9237512774L); ba.setEmailId("swapnilbagul0@gmail.com");
	 * validateInputDetails(ba); }
	 */
}
