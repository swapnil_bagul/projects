package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.app.config.DatabaseConfiguration;
import com.app.pojo.Transactions;

public class TransactionRepository {

	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	PreparedStatement pst = null;

	public Double getCurrentAccountBalance(Long accountNumber) throws ClassNotFoundException, SQLException {
		Double accountBalance = 0.0;
		if (con == null || con.isClosed()) {
			con = DatabaseConfiguration.getDbConnection();
		}

		String sql = "select balance from transactions where accountNumber=" + accountNumber
				+ " order by transactionId desc limit 1;";
		st = con.createStatement();
		rs = st.executeQuery(sql);
		while (rs.next()) {
			accountBalance = rs.getDouble("balance");
		}
		DatabaseConfiguration.closeConnection(st, rs, con);
		return accountBalance;
	}

	public Integer transferAmmountToOtherAccount(Transactions txn) throws ClassNotFoundException, SQLException {
		String msg = "";
		if (con == null || con.isClosed()) {
			con = DatabaseConfiguration.getDbConnection();
		}
		String sql = "insert into transactions (transactionDate,balance,accountNumber,trans_details) values(?,?,?,?)";
		pst = con.prepareStatement(sql);
		pst.setString(1, txn.getTrasactionDate());
		pst.setDouble(2, txn.getAccountBalance());
		pst.setLong(3, txn.getAccount());
		pst.setString(4, txn.getTrans_details());
		Integer i = pst.executeUpdate();
		DatabaseConfiguration.closeConnection(pst, rs, con);
		return i;
	}

	public List<Transactions> getAllTransactionDetails(Long accountNumber) throws ClassNotFoundException, SQLException {
		List<Transactions> txnList = new ArrayList<>();
		if (con == null || con.isClosed()) {
			con = DatabaseConfiguration.getDbConnection();
		}
		String sql = "select *from transactions where accountNumber=" + accountNumber + " order by transactionId";
		st = con.createStatement();
		rs = st.executeQuery(sql);
		while (rs.next()) {
			Transactions txn = new Transactions();
			txn.setTransactionId(rs.getInt("transactionId"));
			txn.setTrasactionDate(rs.getString("transactionDate"));
			txn.setAccountBalance(rs.getDouble("balance"));
			txn.setAccount(rs.getLong("accountNumber"));
			txn.setTrans_details(rs.getString("trans_details"));
			txnList.add(txn);
		}
		DatabaseConfiguration.closeConnection(pst, rs, con);
		return txnList;
	}

	public Integer updateContactDetails(Long accountNumber, String city, Long moNum)
			throws ClassNotFoundException, SQLException {
		if (con == null || con.isClosed()) {
			con = DatabaseConfiguration.getDbConnection();
		}
		String sql = "update bank_account set mobileNumber=" + moNum + ",city='" + city + "' where accountNumber="
				+ accountNumber;
		st = con.createStatement();
		Integer i = st.executeUpdate(sql);
		DatabaseConfiguration.closeConnection(pst, rs, con);
		return i;
	}
}
