package com.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.app.pojo.BankAccount;
import com.app.pojo.Transactions;
import com.app.service.BankAccountService;
import com.app.service.TransactionService;

public class InitClass {
	static int i = 0;
	static List credList = new ArrayList();

	public static void createBankAccount(BankAccountService bankService, Scanner scanner) {
		System.out.println("First Name :");
		String name = scanner.next();
		System.out.println("password :");
		String password = scanner.next();
		System.out.println("Email Id :");
		String emailId = scanner.next();
		System.out.println("Mobile Number :");
		Long mobileNumber = scanner.nextLong();
		System.out.println("Date of Birth :");
		String dob = scanner.next();
		System.out.println("Pan Card :");
		String panCard = scanner.next();
		System.out.println("Aadhar Card :");
		Long adharCard = scanner.nextLong();
		System.out.println("City :");
		String address = scanner.next();
		BankAccount bankAccount = new BankAccount(name, 0L, password, emailId, mobileNumber, dob, panCard, adharCard,
				address);
		Long accountNumber = bankService.createNewBankAccount(bankAccount);
		System.out.println("Bank Account created successfully : Your Bank Account Number is :" + accountNumber);
	}

	public static void loginSystem(BankAccountService bankService, Scanner scanner) {
		if (i == 0) {
			credList = getAccountNumberPassword(scanner);
		}
		Long accountNumber = (Long) credList.get(0);
		String password = (String) credList.get(1);
		Map<Integer, String> loginMenu = bankService.validateAccountHolder(accountNumber, password);
		loginMenu.entrySet().forEach(e -> System.out.println(e.getKey() + ":" + e.getValue()));
		Integer selection = scanner.nextInt();
		TransactionService txnService = new TransactionService();

		if (selection == 1) {
			Double accountBalance = txnService.getCurrentAccountBalance(accountNumber);
			System.out.println("The account balance is : " + accountBalance);
			System.out.println("Enter 1 for menu");
			i = scanner.nextInt();
			if (i == 1)
				loginSystem(bankService, scanner);
			else
				System.out.println("wrong input...!");

		} else if (selection == 2) {
			System.out.println("Enter To account number :");
			Long toAccountNumber = scanner.nextLong();
			System.out.println("Enter ammount to transfer :");
			double ammount = scanner.nextDouble();
			txnService.transferAmmountToOtherAccount(accountNumber, toAccountNumber, ammount);
			System.out.println("Enter 1 for menu");
			i = scanner.nextInt();
			if (i == 1)
				loginSystem(bankService, scanner);
			else
				System.out.println("wrong input...!");
		} else if (selection == 3) {
			txnService.getAllTransactionDetails(accountNumber);
			System.out.println("Enter 1 for menu");
			i = scanner.nextInt();
			if (i == 1)
				loginSystem(bankService, scanner);
			else
				System.out.println("wrong input...!");
		} else if (selection == 4) {
			System.out.println("Enter new mobile number :");
			Long moNum = scanner.nextLong();
			System.out.println("Enter City:");
			String city = scanner.next();
			txnService.updateContactDetails(accountNumber, city, moNum);
			System.out.println("Enter 1 for menu");
			i = scanner.nextInt();
			if (i == 1)
				loginSystem(bankService, scanner);
			else
				System.out.println("wrong input...!");
		}
	}

	private static List getAccountNumberPassword(Scanner scanner) {
		System.out.println("Enter the account number :");
		Long accountNumber = scanner.nextLong();
		System.out.println("Enter the password:");
		String password = scanner.next();
		return new ArrayList(Arrays.asList(accountNumber, password));
	}
}
