package com.app.service;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import com.app.config.DatabaseConfiguration;
import com.app.dao.BankAccountRepository;
import com.app.exception.BankAccountException;
import com.app.pojo.BankAccount;

public class BankAccountService {

	private BankAccountRepository bankAccountRepo = new BankAccountRepository();

	public Map<Integer, String> getMenuService() {
		Map<Integer, String> menuMap = new LinkedHashMap<>();
		// BankAccountRepository bankAccountRepo = new BankAccountRepository();
		try {
			menuMap = bankAccountRepo.getMenu();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return menuMap;

	}

	public Long createNewBankAccount(BankAccount bankAccount) {
		Long accountNumber = null;
		try {
			bankAccount.setAccountNumber(new Random().nextLong(10000000000L));
			accountNumber = bankAccountRepo.createNewAccount(bankAccount);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (BankAccountException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return accountNumber;

	}

	public Map<Integer, String> validateAccountHolder(Long accountNumber, String password) {
		Map<Integer, String> loginMenu = new LinkedHashMap<>();
		try {
			boolean exist = bankAccountRepo.validateBankHolder(accountNumber, password);
			if (exist) {
				System.out.println("\n Logged In Success===WelCome Back===");
				loginMenu = bankAccountRepo.loadLoginMenu();
			} else {
				throw new BankAccountException("The account doesn't exist".toUpperCase());
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return loginMenu;
	}

}
