package com.app.service;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.app.dao.TransactionRepository;
import com.app.exception.BankAccountException;
import com.app.pojo.Transactions;

public class TransactionService {

	private TransactionRepository txnRepo = new TransactionRepository();

	public Double getCurrentAccountBalance(Long accountNumber) {
		Double accountBalance = 0.0;
		try {
			accountBalance = txnRepo.getCurrentAccountBalance(accountNumber);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getException());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return accountBalance;
	}

	public void transferAmmountToOtherAccount(Long fromAccountNumber, Long toAccountNumber, double ammount) {
		try {
			Double fromAccountBalance = getCurrentAccountBalance(fromAccountNumber);
			Double toAccountBalance = getCurrentAccountBalance(toAccountNumber);
			if (fromAccountBalance <= ammount) {
				throw new BankAccountException("You dont have balance to make the transfer");
			} else {
				Transactions txn1 = new Transactions();
				txn1.setAccount(fromAccountNumber);
				txn1.setTrasactionDate(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
				Double bal = fromAccountBalance - ammount;
				txn1.setAccountBalance(bal);
				txn1.setTrans_details(ammount + " debited");
				Integer i = txnRepo.transferAmmountToOtherAccount(txn1);
				txn1.setAccount(toAccountNumber);
				bal = toAccountBalance + ammount;
				txn1.setAccountBalance(bal);
				txn1.setTrans_details(ammount + " credited");
				Integer j = txnRepo.transferAmmountToOtherAccount(txn1);
				if (i > 0 && j > 0) {
					System.out.println("The ammount transfer successful".toUpperCase());
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getException());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void getAllTransactionDetails(Long accountNumber) {
		List<Transactions> txnList = null;
		try {
			txnList = txnRepo.getAllTransactionDetails(accountNumber);
			if (txnList.size() == 0) {
				throw new BankAccountException("No Transactions found".toUpperCase());
			} else {
				txnList.forEach(e -> System.out.println(e));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getException());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void updateContactDetails(Long accountNumber, String city, Long moNum) {
		try {
			Integer i = txnRepo.updateContactDetails(accountNumber, city, moNum);
			if (i > 0) {
				System.out.println("The contact details updated..".toUpperCase());
			} else {
				throw new BankAccountException("Unable to update the contact details");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getException());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}
