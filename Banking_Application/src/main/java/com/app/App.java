package com.app;

import java.util.Map;
import java.util.Scanner;

import com.app.pojo.BankAccount;
import com.app.service.BankAccountService;

public class App {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		BankAccountService bankService = new BankAccountService();
		Map<Integer, String> menuMap = bankService.getMenuService();
		System.out.println("Please select option from below Menu:");
		menuMap.entrySet().forEach(e -> System.out.println(e.getKey() + ":" + e.getValue()));
		Integer selection = scanner.nextInt();
		if (selection == 1) {
			InitClass.loginSystem(bankService, scanner);
		} else if (selection == 2) {
			InitClass.createBankAccount(bankService, scanner);
		} else if (selection == 3) {
			System.out.println("THANK YOU FOR USING OUR SERVICE...!");
		}
	}
}
