package day1.methodchaining;

/*b) method chaining with two different classes
met1(), met2() in some class
met3(), met4() in different class*/

public class MethodChaining2Test {
	public static void main(String[] args) {
		new MethodChaining2Test().method1().method2().method3().method4();
	}

	public MethodChaining2Test method1() {
		System.out.println("inside MethodChaining2Test method1");
		return this;
	}

	public MethodChain method2() {
		System.out.println("inside MethodChaining2Test method2");
		return new MethodChain();
	}

}

class MethodChain {

	public MethodChain method3() {
		System.out.println("inside MethodChain method3");
		return this;
	}

	public MethodChain method4() {
		System.out.println("inside MethodChain method4");
		return this;
	}

}

//output : 

//inside MethodChaining2Test method1
//inside MethodChaining2Test method2
//inside MethodChain method1
//inside MethodChain method2