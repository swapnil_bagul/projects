package day1.methodchaining;

/*3. Provide an example for method chaining using this keyword
a) obj.met1().met2().met3().met4();*/
public class MethodChaining1Test {

	public static void main(String[] args) {
		new MethodChaining1Test().method1().method2().method3();
	}

	public MethodChaining1Test method1() {
		System.out.println("inside method1");
		return this;
	}

	public MethodChaining1Test method2() {
		System.out.println("inside method2");
		return this;
	}

	public MethodChaining1Test method3() {
		System.out.println("inside method3");
		return this;
	}
}

//output:

//inside method1
//inside method2
//inside method3
