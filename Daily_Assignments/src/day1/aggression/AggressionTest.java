package day1.aggression;
//Aggregation example
public class AggressionTest {
	public static void main(String[] args) {
		Driver driver1 = new Driver("swapnil");
		Driver driver2 = new Driver("amar");

		Vehicle veh1 = new Vehicle("Bus", driver1);
		Vehicle veh2 = new Vehicle("Auto", driver2);

		System.out.println(veh1);
		System.out.println(veh2);
	}
}

class Vehicle {
	private String vehicleType;
	private Driver driver;

	public Vehicle(String vehicleType, Driver driver) {
		super();
		this.vehicleType = vehicleType;
		this.driver = driver;
	}

	@Override
	public String toString() {
		return "Vehicle : vehicleType=" + vehicleType + "," + driver;
	}

}

class Driver {
	private String driverName;

	public Driver(String driverName) {
		super();
		this.driverName = driverName;
	}

	@Override
	public String toString() {
		return " driverName=" + driverName;
	}

}