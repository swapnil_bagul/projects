package day1.stackoverflow;

/*1. a) Call main() method from itself, print number of times successfuly called before StackOverflowException, 
 
b) increase Stack size, when running, see that above number gets changed??*/
public class StackOverFlowTest {

	public static void main(String[] args) {
		myMethod(1);
	}

	private static void myMethod(int i) {
		int j = 0;
		try {
			myMethod(++i);
		} catch (StackOverflowError e) {
			System.out.println("StackOverFlow error : " + i);
		}
	}

}

//Output:
//1)Without increasing stack size
//StackOverFlow error : 9663

//2)After increasing stack size
//StackOverFlow error : 8694333
