package day1.composition;

//Complete the example for Composition

public class CompositionTest {

	public static void main(String[] args) {
		University university = new University();
		Departments[] departments = university.getDepartments();
		for (Departments dept : departments) {
			System.out.println(dept.getDeptName());
		}
	}
}

class University {
	private Departments[] departments;

	public University() {
		departments = new Departments[5];
		departments[0] = new Departments("IT");
		departments[1] = new Departments("Finance");
		departments[2] = new Departments("Manufacturing");
		departments[3] = new Departments("Banking");
		departments[4] = new Departments("Pharma");
	}

	public Departments[] getDepartments() {
		return departments;
	}

}

class Departments {

	private String deptName;

	public Departments(String deptName) {
		super();
		this.deptName = deptName;
	}

	public String getDeptName() {
		return deptName;
	}

}