package day1.outofmemory;
//2. Write a program, to make Heap full? Change Heapsize and recheck
public class OutOfMemoryTest {
	public static void main(String[] args) {
		int i = 0;
		A[] arr = new A[10000];
		try {
			for (; i < arr.length; i++) {
				arr[i] = new A();
			}
		} catch (OutOfMemoryError e) {
			System.out.println("Out of Memory error : " + i);
		}
	}
}

class A {
	private A[] arr = new A[100000];
}

//Output:
//1)Without increasing Heap size
//Out of Memory error : 8213


//2)After increasing Heap size
//Out of Memory error : 10086