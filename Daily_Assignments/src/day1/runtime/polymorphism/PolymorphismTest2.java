package day1.runtime.polymorphism;

import com.app.AndroidApplication;

public class PolymorphismTest2 {
	public static void main(String[] args) {
		AndroidApplication app = new GPSApp();
		app.open();
		app.close();
	}
}

class GPSApp extends AndroidApplication {

	@Override
	public void open() {
		System.out.println("GPSApp open() method");
	}

	@Override
	public void close() {
		System.out.println("GPSApp close() method");
	}
}