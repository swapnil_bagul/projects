package day1.runtime.polymorphism;

//Reuse below snippets, and show working of run-time polymorphism
public class PolymorphismTest {

	public static void main(String[] args) {
		Application app = new TodoApp();
		app.open();
		app.close();
	}
}

class Application {

	public void open() {
		System.out.println("Application open() method");
	}

	public void close() {
		System.out.println("Application close() method");
	}

}

class TodoApp extends Application {

	@Override
	public void open() {
		System.out.println("TodoApp open() method");
	}

	@Override
	public void close() {
		System.out.println("TodoApp close() method");
	}
}
