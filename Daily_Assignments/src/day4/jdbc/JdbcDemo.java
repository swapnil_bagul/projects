
//1. In given JDBC program make change to use PreparedStatement instead of Statement?

package day4.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcDemo {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// 1 Load jdbc driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		// 2 Connect to DB
		Connection ct = DriverManager.getConnection("jdbc:mysql://localhost:3306/accountDb", "root", "root");
		// 3 Create Statement
		PreparedStatement st = ct.prepareStatement("select * from accounts");
		// 4 execute statement
		ResultSet rs = st.executeQuery();

		while (rs.next()) {
			System.out.println(rs.getInt("id") + " : " + rs.getLong("account") + " : " + rs.getDouble("ammount"));
		}

	}
}
