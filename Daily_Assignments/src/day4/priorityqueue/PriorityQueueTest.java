package day4.priorityqueue;

import java.util.PriorityQueue;

public class PriorityQueueTest {
	public static void main(String[] args) {

		PriorityQueue<CustomerRequest> pq = new PriorityQueue<>((a, b) -> b.getTurnOver() - a.getTurnOver());
		pq.add(new CustomerRequest("TCS", 650));
		pq.add(new CustomerRequest("Infosys", 430));
		pq.add(new CustomerRequest("IRIS", 980));
		pq.add(new CustomerRequest("Nestle", 420));

		for (CustomerRequest cr : pq) {
			System.out.println(cr);
		}
	}

}

class CustomerRequest {
	private String company;
	private int turnOver;

	public CustomerRequest(String company, int turnOver) {
		super();
		this.company = company;
		this.turnOver = turnOver;
	}

	public String getCompany() {
		return company;
	}

	public int getTurnOver() {
		return turnOver;
	}

	@Override
	public String toString() {
		return "customerRequest [company=" + company + ", turnOver=" + turnOver + "]";
	}

}