package day3.cloning;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

//Example of shallow copy
//This is default way of cloning where we need to implement the class with Clonable marker interface,
//and need to override the clone method of Object class with its default implementation.

public class CloneTest {
	public static void main(String[] args) throws CloneNotSupportedException {

		Employee emp = new Employee("Swapnil", new Company("IRIS Software"));

		// cloning with default clone method of Object class
		Employee cloneEmployee = emp.clone();

		System.out.println(emp.equals(cloneEmployee));
	}
}

class Employee implements Cloneable {
	private String empName;
	private Company company;

	public Employee(String empName, Company company) {
		super();
		this.empName = empName;
		this.company = company;
	}

	public String getEmpName() {
		return empName;
	}

	public Company getCompany() {
		return company;
	}

	@Override
	protected Employee clone() throws CloneNotSupportedException {
		// // TODO Auto-generated method stub
		// return (Employee) super.clone();

		Employee employee = (Employee) super.clone();
		employee.company = this.company.clone();
		return employee;

//		List list = new ArrayList();
//		list.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
	}

//	@Override
//	public boolean equals(Object obj) {
//		if (obj == this)
//			return true;
//		if (obj == null)
//			return false;
//		if (!(obj instanceof Employee))
//			return false;
//		Employee employee = (Employee) obj;
//		return this.getEmpName().equals(employee.getEmpName()) && 
//				this.getCompany().getCompanyName().equals(employee.getCompany().getCompanyName());
//	}

}

class Company implements Cloneable {
	String companyName;

	public Company(String companyName) {
		super();
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	@Override
	protected Company clone() throws CloneNotSupportedException {

		return (Company) super.clone();
	}

}
