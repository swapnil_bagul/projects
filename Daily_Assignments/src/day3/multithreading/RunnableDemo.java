package day3.multithreading;
// Change example to use Runnable for multithreading instead of extending Thread.

//Creating Thread Using Runnable interface.
//Used parameterized constructor to set the instance of Runnable and name of threads.
//new methods used : join(),isAlive(),getState()
public class RunnableDemo {

	public static void main(String[] args) throws InterruptedException {
		int number = 12;
		Thread.currentThread().setName("MainThread");// changing name of main thread to MainThread
		// use the parameterize Thread class constructor to create thread
		// public Thread(instance of Runnable,name of thread)
		Thread thread1 = new Thread(new MyRunnableThread(20), "MyRunnableThread1");
		// starting both the threads using start method.
		thread1.start();

		// The join method will stop the execution of parent thread till the execution
		// of current thread gets over.
		// this is another way of achieving synchronization.remove the below line to
		// check inconsistent output.
		thread1.join();
		System.out.println("====Thread execution is completed====");

		System.out.println("printing table of number" + number);
		for (int i = 0; i < 5;) {
			System.out.println(++i + " : " + Thread.currentThread().getName() + " : " + number * i);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}

		System.out.println(thread1.getName() + "| Thread State : " + thread1.getState() + " | Thread Alive : "
				+ thread1.isAlive());
		System.out.println(Thread.currentThread().getName() + "| Thread State : " + Thread.currentThread().getState()
				+ " | Thread Alive : " + Thread.currentThread().isAlive());

	}
}

class MyRunnableThread implements Runnable {

	private int number;

	public MyRunnableThread(int number) {
		super();
		this.number = number;
	}

	// run() method from thread class is overridden below to have our functionality.
	@Override
	public void run() {
		System.out.println("printing table of number" + number);
		for (int i = 0; i < 5;) {
			System.out.println(++i + " : " + Thread.currentThread().getName() + " : " + number * i);
			// use currentThread() static method of thread class to have the current thread
			// instance.
			// call the getName() method from current thread instance to have the thread
			// name.
			try {
				Thread.sleep(500);// sleep() static method of thread class will sleep the current thread for given
									// millisec(here 200 ms)
				// The sleep method throws the InterruptedException which needs to be handle.
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}

		System.out.println(Thread.currentThread().getName() + "| Thread State : " + Thread.currentThread().getState()
				+ " | Thread Alive : " + Thread.currentThread().isAlive());

	}
}
